<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ page import="java.net.URLDecoder"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="google" content="notranslate">

<title>WMC BMS</title>

<!-- css -->
<link rel="shortcut icon" href="${contextPath}/favicon.ico">
<link rel="STYLESHEET" type="text/css" href="${contextPath}/LOGIN/login.css">

<!-- js -->
<script>var contextPath = "${contextPath}"</script>
<script src="${contextPath}/scripts/jquery-1.10.2.min.js"></script>
<script src="${contextPath}/scripts/jquery.cookie.js"></script>
<script src="${contextPath}/scripts/jquery.timer.js"></script>
<script src="${contextPath}/scripts/jquery.i18n.properties-1.0.9.js"></script>

<!-- global variable -->
<script>
var g_fromGb 			= "WEB";
var g_masterChurchNo 	= "";

var setTime = function() {
	$("#curtime").html(function() {
		var d = new Date();
		//2014년 12월 15일 오후 10시 30분
		var h = d.getHours();
		var ampm;
		if( h >= 12) {
			h = h - 12;
			if( h == 0 )
				h = 12;
			ampm = "PM";
		} else {
			ampm = "AM";
		}

		var day = d.getDay();
		switch(day) {
		case 0:
			day = "Sunday";
			break;
		case 1:
			day = "Monday";
			break;
		case 2:
			day = "Tuesday";
			break;
		case 3:
			day = "Wednesday";
			break;
		case 4:
			day = "Thursday";
			break;
		case 5:
			day = "Friday";
			break;
		case 6:
			day = "Saturday";
			break;
		}

		return day + ", " + (d.getMonth() + 1) + " - " + d.getDate() + ", " + d.getFullYear() + ". " + ampm + " " + h + " : " + d.getMinutes() + " : " + d.getSeconds();
	});
};
</script>

</head>
<body>
<div id="loginwrap">
	<div id="header">
		<h3>WMC 업무 관리시스템</h3>
		<h1>Business Management System</h1>
		<div class="line"><span></span></div>
		<div class="time"><img src="LOGIN/images/time.png"><span id="curtime"></span></div>
	</div>
	

	<div id="main">
		<fieldset>
		<legend>LOGIN</legend>		
		<ul>
			<!-- <li><input type="text" id="name" placeholder="Name" class="name"></li> -->
			<li><input type="text" id="id" placeholder="ID" class="id" value=""></li>
			<li><input type="password" id="pass" placeholder="Password" class="password"></li>
			<li><input type="password" id="common" placeholder="Common Key or Life No." class="commonkey"></li>
			<li><input type="button" id="login" value="LOGIN" class="login"></li>
		</ul>
		<div class="saveid">
			<input type="checkbox" name="chkbox" value="checkbox" id="saveid">
			<label for="saveid"><!-- 이름 /  -->Save ID</label>
		</div>
		</fieldset>
	</div>

	
	<div id="foot">Copyright (c) World Mission Society Church of God. All rights reserved.</div>
</div>

<script>
$(document).ready(function() {
	setTime();
	var timer = $.timer( setTime );
	timer.set({time:1000, autostart : true});
	
	// 다국어 처리
	commonSetLanguage(g_lang);
	commonSetWmcLanguage(g_wmcLang);
	commonLoadMultiLangProperties(commonGetLanguage($.cookie(g_projectName + "LANG")));
	
	// focus 주기
	if(	$("#name").val() == "" ) $("#name").focus();
	else if($("#id").val() == "" ) $("#id").focus();
	else if($("#pass").val() == "" ) $("#pass").focus();
	else if($("#common").val() == "" ) $("#common").focus();

	// id enter pass focus
	$("#id").keyup(function(event) {
		if( event.keyCode == 13 ) { $("#pass").focus(); }
	});

	// pass enter common focus
	$("#pass").keyup(function(event) {
		if( event.keyCode == 13 ) { $("#common").focus(); }
	});

	// common enter lifedispno focus
	$("#common").keyup(function(event) {
		if( event.keyCode == 13 ) { $("#login").click(); }
	});
	
	$("#login").click(function() {
		if(checkControl() == false)
			return;

		onClickLogin($("#id").val(), $("#pass").val(), $("#common").val());
	});
});

function onClickLogin(id, pass, common) {	
	var params = {id:id, pass:pass, common:common, saveId:$("#saveid").is(":checked"), fromGb:g_fromGb, master:g_masterChurchNo};
	commonNewWindow("login_do.wmc", "self", params);
}

function test(){
	alert(11122222233334444);
}

</script>
</body>
</html>
